# Some variables

variable "aws_access_key" {
  description = "AWS access key"
}
variable "aws_secret_key" {
  description = "AWS secret key"
}
variable "aws_region" {
  description = "AWS region"
  default = "eu-north-1"
}

# The default provider configuration

provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key

  region = var.aws_region
}

# The domain

resource "aws_route53_zone" "dynamist-net" {
  name    = "dynamist.net"
  comment = "Managed by Terraform"
}

# A records

resource "aws_route53_record" "dynamist-net" {
  zone_id = aws_route53_zone.dynamist-net.zone_id
  type    = "A"
  name    = ""
  records = ["127.1.2.3"]

  ttl = "300"
}

resource "aws_route53_record" "ci-dynamist-net" {
  zone_id = aws_route53_zone.dynamist-net.zone_id
  type    = "A"
  name    = "ci"
  records = ["127.1.2.3"]

  ttl = "300"
}
